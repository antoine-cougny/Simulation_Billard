#!/usr/bin/python
# -*- coding: Utf-8 -*

''' 
Création du plateau
@author = antoine
@date = Fri 04/05/2016
'''

class Player():
    """
    Noeud qui servira à identifier les joueurs
    """
   
    nombre_joueur_cree = 0      # Variable de classe suffisament explicite

    def __init__(self, nom = 'Jackie 1'):
        Player.nombre_joueur_cree += 1
        self.numero = Player.nombre_joueur_cree # On incrémente automatique le numero du joueur en fonction de son rang de création
        self.next = None
        self.nom = nom

        if Player.nombre_joueur_cree%2 == 0 : self.numero_boule = 2 # Définiton de l'équipe
        else : self.numero_boule = 1

    def __str__(self):
        return("Voici les informations du joueur %i (total %i): \n \t \t Nom : %s \n \t \t Équipe : %i"%(self.numero, Player.nombre_joueur_cree, self.nom, self.numero_boule))


class ListPlayers():
    """
    Liste chainée qui contient la liste des joueurs
    """
    def __init__(self):
        """
        Créer une liste chainée avec le premier joueur
        """     
        self.first = Player()
        self.noeud_courant = self.first


    def firstPlayer(self):
        """
        On se place au début de la liste
        """
        self.noeud_courant = self.first
        return self.noeud_courant
        
    def nextPlayer(self):
        """
        Pour passer au joueur suivant sur la liste chainée
        """
        self.noeud_courant = self.noeud_courant.next
        return self.noeud_courant
    
    def currentPlayer(self):
        return self.noeud_courant

    def __str__(self):
        """
        Décrit la liste chainée de joueurs
        """
        node = self.first
        n = 0
        while  n != Player.nombre_joueur_cree:
            print(node)
            node = node.next
            n += 1
        return("End")

    def addPlayer(self, nom = 0):
        """
        On ajouter un joueur en fin de liste, avec son numero incrémenté automatiquement
        """
        numero = 1          # Compteur
        node = self.first   # Parcours des noeuds
        while node.next != None:
            node = node.next
            numero += 1
        if type(nom) != str: nom = str('Jackie ' + str(numero +1)) # Si un nom 
                                    # n'est pas donné lors du début du jeu, 
                                    # les joueurs se voient attribuer un pseudo automatique
        new_player = Player(nom)
        node.next = new_player
    
        
    def makePlayerReady(self):
        """
        On fait une liste chainée circulaire pour les joueurs
        """ 
        node2 = self.first                              # On va parcourir tous les noeuds
        while node2.next != None:                       # On se place sur le dernier joueur créé
            node2 = node2.next
        
        if Player.nombre_joueur_cree % 2 == 0:
            node2.next = self.first
        elif Player.nombre_joueur_cree % 2 == 1:         # On complète l'équipe 2 si besoin
            print("\n On complete l'équipe 2 (parité) \n")
            new_player = Player(nom = "ExtraPlayer (complète l'équipe 2)")
            node2.next = new_player
            new_player.next = self.first                 # On ferme la liste pour la rendre circulaire
        


if __name__ == '__main__':
    space = "\n############################################################ \n"
    print(space)

    liste_joueur = ListPlayers()
    n = 3
    for k in range(n):
        liste_joueur.addPlayer()
    print(liste_joueur)
    
    print(space)
    liste_joueur.makePlayerReady()
    print(liste_joueur)
    
    print(space)
    node = liste_joueur.firstPlayer()
    
    print(node)
    for k in range(7):
        print(liste_joueur.nextPlayer())

    print("\n On a bien une liste circulaire")
    print(space)
