#!/usr/bin/python3
# -*- coding: Utf-8 -*

''' 
Création du plateau
@author = antoine & aymeric
@date = Wed 10/02/2016
'''

from constantes import *
from Boule import *
#from Verre import *
from random import randint
from math import sqrt, cos, sin

class Plateau():
    """
    Plateau 
    Ces caractéristiques sont sa taille (hauteur, largeur), les boules présentes 
    dessus et éventuellement les verres à éviter. Le plateau possède également 
    deux listes qui servent à compter les points dans une partie.
    """
    def __init__(self, largeur, hauteur):
        self.__hauteur = hauteur
        self.__largeur = largeur
        self.boules = []            # contient les boules présentes sur le plateau
        self.verres = []            # contient les verres présents sur le plateau
        self.contact = [False, False, False] # Liste qui va servir pour compter les points au billard français
        self.points = [0,0]         # Liste qui contient les points des deux équipes 
    
    def __str__(self):
        return 'Hauteur = {0}, largeur = {1}, nombre de boules = {2}, nombre de verres = {3}'.\
        format(self.hauteur, self.largeur, len(self.boules), len(self.verres))

    @property
    def hauteur(self):
        return(self.__hauteur)

    @property
    def largeur(self):
        return(self.__largeur)

    def ajouterBoules(self,x, y, couleur, aleatoire = False, vx = 0, vy = 0):
        """
        Ajoute une boule au plateau
        Arguments : couleur : str
                    aleatoire : booléen qui sert pour les tests
                    vx, vy : int qui sont forcés pour les tests
        """
        if aleatoire is True:
            x = randint(int(1 + diametre), int(self.largeur - diametre))
            y = randint(int(1 + diametre), int(self.largeur - diametre))
            vx, vy = randint(1, 25), randint(1, 25) 
        
        B = Boule(x, y, vx, vy, diametre, couleur)
        
        self.boules.append(B)
        return(B)

    def ajouterVerre(self, taille, pieds): 
        """
        Ajoute un verre au plateau, verre au pieds 'carre' ou 'cercle'
        Arguments : taille, int et pieds, str
        """
        x = randint(int(1 + taille), int(self.largeur - taille))
        y = randint(int(1 + taille), int(self.largeur - taille))
        V = Verre(x, y, taille, pieds)
        self.verres.append(V)
        
        return(V)

    def detecterBords(self, B, cst = 5):
        """
        Détecte les collisions avec le bord du plateau et fait rebondir les boules en conséquence
        """
        if B.y >=  (self.largeur - rayon) - cst or B.y <= rayon + cst : 
            B.set_vy = - B.vy
        elif B.x >= (self.hauteur - rayon) - cst or B.x <= rayon + cst :
            B.set_vx = - B.vx

    def detecterCollisionBoules(self, B1, B2, numero_equipe):
        """
        Test si deux boules sont en contact et les fait rebondir si c'est le cas
        Arguments : B1, B2 : les deux boules
                    numero_equipe : int, pour le comptage des points
        """
        if sqrt((B1.y-B2.y)**2+(B1.x-B2.x)**2) <= diametre + 5: #toutes les boules ont le meme diametre, sinon faire (B1.rayon + B2.rayon)
            self.historiqueContact(B1, B2, numero_equipe)
            n1, g1, n2, g2 = B1.projection2(B2)
            n1, g1, n2, g2 = n2, g1, n1, g2
            B1.set_vx = n1[0] + g1[0]
            B1.set_vy = n1[1] + g1[1]
            B2.set_vx = n2[0] + g2[0]
            B2.set_vy = n2[1] + g2[1]
            print("contact1")
            #print("B1.vy=", B1.vy)
            #print("n1[1] + g1[1]=",n1[1] + g1[1])

    def frapperBoule(self, B, angle, force):
        """
        La tige frappe la boule avec une certaine *force* selon une *direction*
        
        Arguments : direction : vecteur de longueur 2
                    force : float
        """
        direction = [-cos(angle), sin(angle)]
        B.set_vx += direction[0]*force
        B.set_vy += direction[1]*force

    def historiqueContact(self, B1, B2, numero_equipe):
        """
        Met à jour le tableau self.contact en fonction des boules qui viennent d'être touchées
        B1, B2 les deux boules du contact
        numero_equipe : int, l'indice de la boule de l'équipe qui joue [normalement ;)]
        """
        i, j = self.boules.index(B1), self.boules.index(B2)
        
        if numero_equipe == 2: numero_equipe = 1
        elif numero_equipe == 1: numero_equipe = 2
        
        self.contact[numero_equipe] = True
        if i == numero_equipe : 
            self.contact[j] = True
            
        elif j == numero_equipe : 
            self.contact[i] = True
            

    def tourSuivant(self, numero_equipe):
        """
        Rénitialise les contacts des boules entre elles pour le tour suivant
        Appel de cette méthode par l'UI
        """
        self.contact = [False, False, False]
        self.contact[numero_equipe] = True          # On met à True le booléen qui correspond au numéro de l'équipe qui joue actuellement

    def ajouterPoint(self, numero_equipe):
        """
        On regarde à la fin du tour si l'équipe qui a joué a gagné un point ou non
        liste_boule : liste de longueur 2
        equipe : numero de l'équipe (1 ou 2 de mémoire)
        """
        if numero_equipe == 2: numero_equipe = 1
        elif numero_equipe == 1: numero_equipe = 2
        if not(False in self.contact):
            self.points[numero_equipe -1] += 1
            print(self.points)
            return True
        else: 
            return False
    
    def testMouvement(self):
        """
        Retourne True si une des boules du terrain possède toujours une vitesse
        selon x ou y pour continuer de mettre à jour l'interface graphique
        """
        for boule in self.boules:
            if boule.vx > 0.001 or boule.vy > 0.001:
                return True

    def mettreAJour(self, numero_equipe = 0, coef = 0.98, cst = 0):
        """
        Met le plateau à jour à l'instant suivant
        -> check collisions avec bord / autre boule / objet posés
        -> met a jour l'etat de la boule
        """
        n = len(self.boules)
        for k in range(n):
            self.boules[k].mettreAJour(coef)                # Met à jour la boule
            self.detecterBords(self.boules[k], cst)         # Check les collisions avec le bord
            print(self.points)
            for j in range(k+1, n):
                self.detecterCollisionBoules(self.boules[k], self.boules[j], numero_equipe)

if __name__ == "__main__":
    tapis = Plateau(400,200)
    nb_boule = int(sys.argv[1])
    nbtour = int(sys.argv[2])
    for i in range(nb_boule):
        tapis.boules.append(tapis.ajouterBoules("blanche"))
    for t in range(nbtour):
        print("### Tour %i ###"%(t))
        tapis.mettreAJour()
        print(tapis)
        for k in range(nb_boule):
            print(tapis.boules[k])
