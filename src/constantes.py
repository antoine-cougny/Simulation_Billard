#!/usr/bin/python
# -*- coding: Utf-8 -*

''' 
Constantes utilisées dans le projet
@author = antoine
@date = Wed 10/02/2016
'''

diametre = int(61)	    #mm
xmax = int(3100 / 3)
ymax = int(1677 / 3)
masse = 209             #g
g = 9,80665
rayon = diametre/2
L = ymax
H = xmax

# Position initiale des boules rouge, jaune et blanche
(xr, yr) = (xmax/4, ymax/2)
(xj, yj) = (3*xmax/4, ymax/2)
(xb, yb) = (3*xmax/4, ymax/4)

numero_joueur = 1


# Seule fonction du projet
def produitScalaire(vect1,vect2):
    """
    Produit scalaire canonique dans Rn
    """
    n=len(vect1)
    if n != len(vect2):
        return "Pb de dimensions"
    else:
        result = 0
        for i in range(n):
            result += vect1[i]*vect2[i]
        return result

