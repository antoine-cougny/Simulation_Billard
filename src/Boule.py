# -*- coding: utf-8 -*-
"""
Created on Wed Feb 10 10:58:13 2016

@author: aymeric
"""
from constantes import *
import sys
from random import randint
from math import sqrt


class Objet:
    """
    Concerne tout les objets posés sur la table du billard
    """
    
    def __init__(self, x=xmax/2, y=ymax/2):
        self.__x = x
        self.__y = y
        
        
    @property
    def x(self):
        return self.__x

    @property
    def y(self):
        return self.__y
    
    @x.setter
    def set_x(self, val):   #depend de l'interface graphique et comment est situé l'origine du repère
        self.__x = val
        
    @y.setter
    def set_y(self, val):
        self.__y = val
    
    def __str__(self):
        return 'pos = ({0}, {1})'.format(self.__x, self.__y)



class Boule(Objet):
    """
    Représente les boules de billard
    """
    
    def __init__(self, x, y, vx, vy, diametre, couleur):
        super().__init__(x, y)
        self.__taille = float(diametre)
        self.__vx = float(vx)
        self.__vy = float(vy)
        self.__couleur = couleur


    def __str__(self):
        return 'Boule : {0}, diametre = {1}, vitesse = ({2}, {3}), couleur = {4}'.\
        format(super().__str__(), self.__taille, self.__vx, self.__vy, self.__couleur)

#    def __str__(self):
#        return 'Boule : (%d, %d), diametre = %f, vitesse = (%f, %f), couleur = %s'%(self.x, self.y, self.taille, self.vx, self.vy, self.couleur)

    @property
    def vx(self):
        return self.__vx

    @property
    def vy(self):
        return self.__vy
   
    @vx.setter
    def set_vx(self, val):
        self.__vx = val
      
    @vy.setter
    def set_vy(self, val):
        self.__vy = val

    @property
    def taille(self):
        return(self.__taille)

    @property #en a t-on vraiment besoin ?
    def couleur(self):
        return(self.__couleur)

    def mettreAJour(self, coef = 1):
        """
        Met à jour l'état de la boule
        Argument : coef : float qui représente les frottements dynamiques du billard
                    self : la boule
        """
        
        self.set_vx = self.vx*coef
        self.set_vy = self.vy*coef
  
        self.set_x = self.x + self.vx
        self.set_y = self.y + self.vy
    
    def vectNormalTangentiel(self, B2):
        """
        Retourne les vecteurs g et n normalisés
        """

        n_aux = [self.x-B2.x, self.y-B2.y]
        norme = sqrt(produitScalaire(n_aux,n_aux))
        g_aux = [B2.y-self.y, self.x-B2.x]
        n = [x * (1/norme) for x in n_aux]
        g = [x * (1/norme) for x in g_aux]

        return(n,g)
 
    def projection2(self,B2):
        """
        Fait la projection des vitesses sur n et g
        """
        (n,g) = self.vectNormalTangentiel(B2)
        
        n1_aux = produitScalaire(n,[self.vx,self.vy])
        g1_aux = produitScalaire(g,[self.vx,self.vy])
        
        n2_aux = produitScalaire(n,[B2.vx,B2.vy])
        g2_aux = produitScalaire(g,[B2.vx,B2.vy])
        
        n1 = [x * n1_aux for x in n] #vitesse de B1 projetée sur n
        g1 = [x * g1_aux for x in g] #vitesse de B1 projetée sur g
        
        n2 = [x * n2_aux for x in n]
        g2 = [x * g2_aux for x in g]
        
        return (n1,g1,n2,g2)


if __name__ == "__main__":
    lst_boule = []
    nb_boule = int(sys.argv[1])
    nbtour = int(sys.argv[2])
    for i in range(nb_boule):
        lst_boule.append(Boule(randint(0, 200), randint(0, 400), randint(0, 10), randint(0,50), diametre, "blanche"))
    for t in range(nbtour):
        print("### Tour %i ###"%(t))
        for b in lst_boule:
            b.mettreAJour()
            print(b)
