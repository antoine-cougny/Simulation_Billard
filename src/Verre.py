#!/usr/bin/python
# -*- coding: Utf-8 -*

''' 
Création du plateau
@author = antoine
@date = Fri 19/02/2016
'''

from constantes import *
from Boule import *

class Verre(Objet):
    """
    Représente les verrres posés sur la table comme obstacles.
    """

    def __init__(self, x, y, taille, pieds):
    	"""
    	pieds = 'carre' ou 'cercle'
    	"""
        if pieds == 'cercle':
                self.__diametre = taille

        elif pieds == 'carre':
                self.__cote = taille
                x = x - taille/2
                y = y - taille/2
        super().__init__(self, x, y)
        self._etat = 'ok'

    def changerEtat(self):
    	self._etat = 'casse'

    def collisionVerre(self, B):
    	"""
    	Renvoie un booléen si oui ou non, une boule touche un verre.
    	"""
    	if self.pieds == 'cercle':
    	        norme = sqrt((B.__x - self.x)**2 + (B.__y - self.y)**2)
    	        if norme < self.__diametre:
                        self.changerEtat()
    	elif self.pieds == 'carre':
    	        #utiliser norme 1
    	        #à voir si on continue la dessus plus tard
    	        pass


    def mettreAJour(self, B):
    	"""
    	Met à jour le verre à l'état suivant
    	check : collision avec une boule
    	"""
    	self.collisionVerre(B)
    	if self.etat == 'casse':
    		#FIN DU JEU
    	        pass
