#!/usr/bin/python3
# -*- coding: Utf-8 -*

''' 
Création du plateau
@author = antoine
@date = Wed 14/04/2016
'''

from tkinter import *
from tkinter.messagebox import *

import math
import sys
sys.path.append("./src")
from Plateau import *
from Boule   import *
from constantes import *
from UIJeu import *

#Aide : # EXERCICE : http://sebsauvage.net/python/gui/index_fr.html

class UI(Tk):
    def __init__(self,parent):
        Tk.__init__(self,parent)
        self.parent = parent # Utile pour montrer/masquer des groupes de widgets, les redessiner à l'écran ou les détruire quand la fenêtre est fermée.
        self.initializeUI()

    def initializeUI(self):
        """
        On créé les éléments graphique séparés du reste du programme
        """
        # Gestionnaire de layout
        self.grid()
        #self.entry = Entry(self)
        #self.entry.grid(column=0,row=0,sticky='EW')

        ####### Frame d'instruction du jeu #######
        instruction = LabelFrame(self, text="Avant de jouer, configuration du jeu", padx=20, pady=20)
        Label(instruction, text="Selectionner le nombre de joueur, leur nom et le mode de jeu").pack()
        instruction.grid(column = 0, columnspan=4, row = 0)


        #### BOUTON RADIO A FINIR


        ####### Choix des règles du jeu #######
        #choix = IntVar()
        #choix_billard_fr = Radiobutton(self, text="Jouer au billard français", variable=choix, value=1).pack(anchor='w')
        #choix_billard_en = Radiobutton(self, text="Jouer au billard américain", variable=choix, value=2).pack(anchor='w')      


        #########
        
        ####### Création des boutons #######
        bouton_nb_joueur = Button(self, text = "Nombre de joueur", activebackground="red", command=self.onBoutonNombreJoueur)
        bouton_nb_joueur.grid(column = 0, row = 3)
        self.nombre_joueur = IntVar()
        
        bouton_nom_joueur = Button(self, text = "Nom des joueurs", activebackground="red", command = self.onBoutonNomJoueur)
        bouton_nom_joueur.grid(column = 1, row = 3)
        
        bouton_quit = Button(self, text = "Quitter", activebackground="yellow", command = self.quit)
        bouton_quit.grid(column = 2, row = 3)
        
        bouton_play = Button(self, text = "Jouer", activebackground='green', command = self.onBoutonJouer)
        bouton_play.grid(column = 3, row = 3)

        ####### Création d'un label 'historique' #######
        self.labelVariable = StringVar()                        #Variable qui va s'actualiser
        label = Label(self, textvariable=self.labelVariable, anchor="w", fg="white", bg="blue")
        label.grid(column=0, row=4, columnspan=4, sticky='EW')
        self.labelVariable.set("Historique")

        self.grid_columnconfigure((0,1),weight=1)               #Redimensionnement automatique (numero colonne, vitesse)
        self.resizable(True,False)                              #Contrainte de redimensionnement (x, y)

        ##### Création d'un champ de texte pour mettre le nombre de joueur à la place ####
        self.entry = Entry(self,textvariable=self.nombre_joueur)
        self.entry.grid(column=0,row=5,sticky='EW')
        self.entry.bind("<Return>", self.onPressEnter)
        self.entry.focus_set()
        self.entry.selection_range(2, END)
        bouton_nb_joueur2 = Button(self, text = "Nombre de joueur 2e tentative", bg="red", command=self.onBoutonNombreJoueurV2)
        bouton_nb_joueur2.grid(column = 1, row = 5,  columnspan=2)
    

    ############################################################################

    def onOKClicked(self):
        self.nb_joueur.destroy
        print(self.nombre_joueur.get())
        self.labelVariable.set("Sélection du nombre de joueur : %i "%(self.nombre_joueur.get()))  

    def onBoutonNombreJoueur(self):
        self.nb_joueur = Tk()
        self.nb_joueur.title("Choix du nombre de joueurs")
        self.nb_joueur.grid()
        #Spinbox pour le nombre de joueur :
        #nb = Spinbox(self.nb_joueur, from_=0, to=10)
        #nb.pack()  

        #self.entryVariable = IntVar()
        self.nb_joueur.entry = Entry(self.nb_joueur,textvariable=self.nombre_joueur)
        self.nb_joueur.entry.focus_set()
        self.nb_joueur.entry.selection_range(2, END)
        self.nb_joueur.entry.grid(column=0, row=0, rowspan = 7,sticky='EW')
        self.nb_joueur.entry.bind("<Return>", self.onOKClicked())
        #self.entryVariable.set(u"Enter text here.")
        
        #bouton = Button(self.nb_joueur, text = "OK", command = self.onOKClicked())
        bouton = Button(self.nb_joueur, text = "OK", command = self.nb_joueur.destroy)
        bouton.grid(column = 1, row = 0)
        #self.nb_joueur.mainloop()
        ## Problème avec le Ok et ou le Enter en fonction de comment je procède
        ## Comment récupérer la valeur qui est dans le box ? ie attendre la nouvelle valeur ?


    ############################################################################


    def onBoutonNombreJoueurV2(self):
        self.labelVariable.set("Sélection du nombre de joueur : %i "%(self.nombre_joueur.get()))

    def alert(self):
        print('en cours de dev')        

    def onBoutonNomJoueur(self):
        self.labelVariable.set("Sélection du nom des joueurs !")

    def onBoutonJouer(self):
        self.labelVariable.set("C'est parti !")
        #player = self.Joueur.initialize
        app2 = UIJeu(self.nombre_joueur.get())
        app.destroy()
        app2.mainloop()
        
        
    def onPressEnter(self,event):
        self.labelVariable.set( self.nombre_joueur.get()+" (Appuie sur ENTER)" )



        
if __name__ == "__main__":
    app = UI(None)
    app.title('Jeu de billard')
    app.mainloop()

    #app2 = UIJeu(None)
    #app2.mainloop()
