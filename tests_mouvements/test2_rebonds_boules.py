#!/usr/bin/python3
# -*- coding: Utf-8 -*

''' 
Création du plateau
@author = antoine & aymeric
@date = Wed 07/05/2016
'''

"""
Ce fichier de test consiste à tester le mouvement des boules sur le plateau de billard sans frottement (coef=1)
BUT : Vérifier que les méthodes mettreAJour et de détection de colision fonctionnent.

ICI on se concentre sur les colisions entre les boules

Param à changer : Ligne 80 (coef et cst=marge) - pour modifier la vitesse initale maximum, voir la methode ajouterBoule de la classe Plateau
"""


from tkinter import *
from tkinter.messagebox import *

import math
import time
import sys
sys.path.append("./../src")
sys.path.append("./src")
from Plateau import *
from Boule   import *
from constantes import *
from Players import *


fenetre = Tk()

k = 2
tapis = Plateau(L, H)
liste_joueur = ListPlayers()

alea = False
tapis.ajouterBoules(xr, yr, couleur = 'Red', aleatoire=alea)
tapis.ajouterBoules(xb, yb, couleur = 'White', aleatoire=alea, vx = 0, vy = 10)
tapis.ajouterBoules(xj, yj, couleur = 'Yellow', aleatoire=alea, vx = 0, vy = -10)

can = Canvas(fenetre, width=xmax, height=ymax, bg='dark green', highlightbackground = 'brown')
can.grid(row=1, column=0, rowspan=4)

boule_rouge = can.create_oval(tapis.boules[0].x-rayon, 
                                        tapis.boules[0].y-rayon, 
                                        tapis.boules[0].x+rayon, 
                                        tapis.boules[0].y+rayon, 
                                        fill=tapis.boules[0].couleur)
boule_blanche = can.create_oval(tapis.boules[1].x-rayon, 
                                        tapis.boules[1].y-rayon, 
                                        tapis.boules[1].x+rayon, 
                                        tapis.boules[1].y+rayon, 
                                        fill=tapis.boules[1].couleur)
boule_jaune = can.create_oval(tapis.boules[2].x-rayon, 
                                        tapis.boules[2].y-rayon, 
                                        tapis.boules[2].x+rayon, 
                                        tapis.boules[2].y+rayon, 
                                        fill=tapis.boules[2].couleur)

liste_boule_can = [boule_rouge, boule_blanche, boule_jaune]


i = 0 # Compteur de tour pour le terminal

while True:
    obj0 = tapis.boules[0]
    obj1 = tapis.boules[1]
    obj2 = tapis.boules[2]

    dx0 = obj0.x
    dy0 = obj0.y
    dx1 = obj1.x
    dy1 = obj1.y
    dx2 = obj2.x
    dy2 = obj2.y

    # On met à jour le tapis
    tapis.mettreAJour(coef=1, cst=10)

    # On récupère les nouveaux objects [est ce vraiment nécessaire ?]
    obj0 = tapis.boules[0]
    obj1 = tapis.boules[1]
    obj2 = tapis.boules[2]

    # Déplacement selon x et y de chacune des boules
    dx0 = obj0.x - dx0
    dy0 = obj0.y - dy0
    dx1 = obj1.x - dx1
    dy1 = obj1.y - dy1
    dx2 = obj2.x - dx2
    dy2 = obj2.y - dy2

    # On bouge les éléments graphiques
    can.move(liste_boule_can[0], dx0, dy0)
    can.move(liste_boule_can[1], dx1, dy1)
    can.move(liste_boule_can[2], dx2, dy2)

    i+=1
    #print(i)
    # On met à jour l'affichage
    fenetre.update()
    fenetre.after(35)

fenetre.mainloop()
